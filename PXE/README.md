These configuration files are for facilitating automatic OS installation 
with PXE. The kickstart and preseed files just need some values filled in
like usernames, passwords and IP addresses. They are intended to be used with
the Ubuntu netboot installer extracted to the ubuntu-installer directory,
the contents of a CentOS 7 Minimal image shared over HTTP, and a local 
(remote should work as well, depending on your internet connection)
apt repository.
