Install required packages: `yum install -y autofs samba-common cifs-utils`

Add the following line to the end of `/etc/auto.master`:
```
/mnt/samba /etc/auto.cifs --timeout=600 --ghost
```
Create `/etc/auto.cifs` with the following contents:
```
share_data -fstype=cifs,rw,noperm,credentials=/etc/smbcreds.txt ://<IP_OF_SMB_SERVER>/<SHARE_NAME>
```
Create `/etc/smbcreds.txt` with the following contents:
```
username=<USERNAME>
password=<PASSWORD>
```
Set proper permissions on `smbcreds.txt`:

`chmod 600 /etc/smbcreds.txt`

Start autofs and set to run on boot:

`systemctl start autofs`

`systemctl enable autofs`
